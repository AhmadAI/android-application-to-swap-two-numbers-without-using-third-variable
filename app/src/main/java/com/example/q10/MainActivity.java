package com.example.q10;

import android.annotation.SuppressLint;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.q10.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn;
        EditText etext1, etext2;
        TextView rtv;

        btn=findViewById(R.id.btn);
        etext1=findViewById(R.id.etext1);
        etext2=findViewById(R.id.etext2);
        rtv=findViewById(R.id.rtv);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num1 = Integer.parseInt(etext1.getText().toString());
                int num2 = Integer.parseInt(etext2.getText().toString());

                num1 = num1+num2;
                num2 = num1-num2;
                num1 = num1-num2;

                rtv.setText("After swapping: "+"\nfirst number = "+num1+"\nsecond number = "+num2);
            }
        });
    }
}